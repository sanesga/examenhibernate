/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package app;

import java.util.Calendar;
import java.util.HashSet;
import java.util.List;
import java.util.Locale;
import java.util.Scanner;
import java.util.Set;
import model.Aula;
import model.Categoria;
import model.Departament;
import model.Nom;
import model.Profesor;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

/**
 *
 * @author Maite
 */
public class App {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        Scanner teclado = new Scanner(System.in);

        int opcion;
        Profesor p1 = null, p2, p3, p4, p5, p6, p7, p8, p9;
        Categoria categoria = null;
        Nom nom1, nom2, nom3, nom4, nom5, nom6, nom7, nom8, nom9;
        Departament departamentAdm = null, departamentBio = null, departamentInf = null, departamentSan = null, departamentTec = null;
        Calendar data = Calendar.getInstance();
        Set<Profesor> profesoresAdm = new HashSet<>();
        Set<Profesor> profesoresBio = new HashSet<>();
        Set<Profesor> profesoresInf = new HashSet<>();
        Set<Profesor> profesoresSan = new HashSet<>();
        Set<Profesor> profesoresTec = new HashSet<>();

        Aula aula;
        Query query;
        List<Object> listaDatos;

        //CREAMOS CONEXION
        SessionFactory sessionFactory;
        Configuration configuration = new Configuration();
        configuration.configure();
        sessionFactory = configuration.buildSessionFactory();
        SessionFactory factory = new Configuration().configure().buildSessionFactory();

        //CREAR UNA SESION
        Session session = factory.openSession();

        do {
            System.out.println("Elige opción");
            menu();
            opcion = teclado.nextInt();

            switch (opcion) {
                case 1://CREAR DEPARTAMENT
                    data.set(2005, 00, 01);

                    departamentAdm = new Departament("Adm", "Administratiu", data.getTime(), 10);
                    departamentBio = new Departament("Bio", "Biologia", data.getTime(), 20);
                    departamentInf = new Departament("Inf", "Informàtica", data.getTime(), 15);
                    departamentSan = new Departament("San", "Sanitària", data.getTime(), 0);
                    departamentTec = new Departament("Tec", "Tecnologia", data.getTime(), 16);

                    //creem els noms dels profesors
                    nom1 = new Nom("J.A.Climent");
                    nom2 = new Nom("Lluis Escriva");
                    nom3 = new Nom("Carles Masanet");
                    nom4 = new Nom("Paqui Ronda");
                    nom5 = new Nom("Xavier Llobell");
                    nom6 = new Nom("Josep Torres");
                    nom7 = new Nom("Anna Bonilla");
                    nom8 = new Nom("Adolfo Alzaraz");
                    nom9 = new Nom("Manoli Climent");

                    p1 = new Profesor(nom1, categoria.definitiu, departamentSan);
                    p2 = new Profesor(nom2, categoria.definitiu, departamentTec);
                    p3 = new Profesor(nom3, categoria.definitiu, departamentBio);
                    p4 = new Profesor(nom4, categoria.interi, departamentInf);
                    p5 = new Profesor(nom5, categoria.definitiu, departamentAdm);
                    p6 = new Profesor(nom6, categoria.definitiu, departamentAdm);
                    p7 = new Profesor(nom7, categoria.interi, departamentInf);
                    p8 = new Profesor(nom8, categoria.interi, departamentInf);
                    p9 = new Profesor(nom9, categoria.interi, departamentAdm);

                    profesoresAdm.add(p5);
                    profesoresAdm.add(p6);
                    profesoresAdm.add(p9);
                    profesoresTec.add(p2);
                    profesoresInf.add(p4);
                    profesoresInf.add(p7);
                    profesoresInf.add(p8);
                    profesoresBio.add(p3);
                    profesoresSan.add(p1);

                    departamentSan.setProfesores(profesoresSan);
                    departamentBio.setProfesores(profesoresBio);
                    departamentTec.setProfesores(profesoresTec);
                    departamentInf.setProfesores(profesoresInf);
                    departamentAdm.setProfesores(profesoresAdm);
                    
                    //creem aula
                    aula = new Aula("Adm", "Aula de administratiu");
                    departamentAdm.setAula(aula);

                    System.out.println("Departaments creados con éxito");
                    break;
                case 2://GUARDAR DEPARTAMENT
                    session.beginTransaction();
                    session.save(departamentAdm);
                    session.save(departamentBio);
                    session.save(departamentInf);
                    session.save(departamentSan);
                    session.save(departamentTec);
                    session.getTransaction().commit();
                    System.out.println("Departament guardado con éxito");
                    break;
                case 3://ACTUALIZAR DEPARTAMENT
                    session.beginTransaction();
                    departamentTec = session.get(Departament.class, "Tec");
                    departamentTec.setHores(20);
                    session.update(departamentTec);
                    session.getTransaction().commit();
                    System.out.println("Departament actualizado con éxito");
                    break;
                case 4://ELIMINAR DEPARTAMENT
                    session.beginTransaction();
                    departamentBio = session.get(Departament.class, "Bio");
                    session.delete(departamentBio);
                    session.getTransaction().commit();
                    System.out.println("Departament eliminado con éxito");
                    break;

                case 5://Seleccionar nombre de los profesores cuyo nombre empieza por A o por X

                    query = session.createQuery("SELECT p.nom FROM Profesor p WHERE p.nom LIKE 'A%' OR p.nom LIKE 'X%'");
                    listaDatos = query.list();
                    for (Object dato : listaDatos) {
                        System.out.println(dato);
                    }
                    break;
                case 6://Selecciona el nombre de los profesores que tienen la misma categoria que el profesor con el número 1
                    
                    query = session.createQuery("SELECT p.nom FROM Profesor p WHERE p.categoria=(SELECT p.categoria FROM Profesor p WHERE p.num=1)");
                    listaDatos = query.list();
                    for (Object dato : listaDatos) {
                        System.out.println(dato);
                    }
                    break;
                case 7://Selecciona los departamentos que tienen más horas que la media
                    
                    query = session.createQuery("SELECT d FROM Departament d WHERE d.hores > (SELECT AVG(d.hores) FROM Departament d)");
                    
                    List<Departament> departaments = query.list();
                    for (Departament departament : departaments) {
                        System.out.println("Codi: "+departament.getCodi());
                         System.out.println("Data: "+departament.getData());
                          System.out.println("Hores: " + departament.getHores());
                           System.out.println("Nom: "+departament.getNom());
                    }
                    break;
                case 8://guarda la consulta en el fichero de mapeo hibernate
                    
                    query = session.getNamedQuery("departamentosMasHoras");
                   List<Departament> departaments2 = query.list();
                    for (Departament departament : departaments2) {
                        System.out.println("Codi: "+departament.getCodi());
                         System.out.println("Data: "+departament.getData());
                          System.out.println("Hores: " + departament.getHores());
                           System.out.println("Nom: "+departament.getNom());
                    }
                    break;
              
                case 20://SALIR
                    //cerrar la conexion
                    session.close();
                    sessionFactory.close();
                    System.out.println("Saliendo..");
                    break;
                default:
                    throw new AssertionError();
            }
        } while (opcion != 20);

    }

    public static void menu() {
        System.out.println("CONSIDERACIÓN 1. EJECUTAR LOS PASOS POR ORDEN");
        System.out.println("CONSIDERACIÓN 2. RECORDAR SALIR DE LA APLICACIÓN PARA CERRAR SESIÓN");
        System.out.println("CONSIDERACIÓN 3. SI SE INTENTA VOLVER A AÑADIR PROFESOR, COMO SON FIJOS DARÁ ERROR");
        System.out.println("1. Crear Departament");
        System.out.println("2. Guardar Departament");
        System.out.println("3. Actualizar Departament");
        System.out.println("4. Eliminar Departament");

        System.out.println("---------CONSULTAS----------");
        System.out.println("5. Seleccionar nombre de los profesores cuyo nombre empieza por A o por X");
        System.out.println("6. Selecciona el nombre de los profesores que tienen la misma categoria que el profesor con el número 1");
        System.out.println("7. Selecciona los departamentos que tienen más horas que la media");
        System.out.println("8. Guarda la consulta en fichero de mapeo hibernate");
        System.out.println("20. Salir");
    }

}
