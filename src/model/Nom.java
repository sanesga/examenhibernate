/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

/**
 *
 * @author Sandra
 */
public class Nom  {
    
    private String nom;

    public Nom(String nom) {
        this.nom = nom;
    }

    public Nom() {
    }

    @Override
    public String toString() {
        return "Nom{" + "nom=" + nom + '}';
    }
    
    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }
    
    
}
