/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import java.io.Serializable;

/**
 *
 * @author Sandra
 */
public class Profesor implements Serializable {
  private int num;
  private Nom nom;
  private Categoria categoria;
  private Departament departament;

     public Profesor(){
     }

    public Profesor(Nom nom, Categoria categoria, Departament departament) {
        this.nom = nom;
        this.categoria = categoria;
        this.departament = departament;
    }

    @Override
    public String toString() {
        return "Profesor{" + "num=" + num + ", nom=" + nom + ", categoria=" + categoria + ", departament=" + departament + '}';
    }

    public int getNum() {
        return num;
    }

    public void setNum(int num) {
        this.num = num;
    }

    public Nom getNom() {
        return nom;
    }

    public void setNom(Nom nom) {
        this.nom = nom;
    }

    public Categoria getCategoria() {
        return categoria;
    }

    public void setCategoria(Categoria categoria) {
        this.categoria = categoria;
    }

    public Departament getDepartament() {
        return departament;
    }

    public void setDepartament(Departament departament) {
        this.departament = departament;
    }
    

}
