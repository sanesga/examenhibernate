/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import java.io.Serializable;
import java.util.Date;
import java.util.Set;

/**
 *
 * @author Sandra
 */
public class Departament  implements Serializable{
    private String codi;
    private String nom;
    private Date data;
    private int hores;
    private Set<Profesor> profesores;
    private Aula aula;

    public Departament(String codi, String nom, Date data, int hores) {
        this.codi = codi;
        this.nom = nom;
        this.data = data;
        this.hores = hores;
    }

    public Departament() {
    }

    @Override
    public String toString() {
        return "Departament{" + "codi=" + codi + ", nom=" + nom + ", data=" + data + ", hores=" + hores + ", profesores=" + profesores + ", aula=" + aula + '}';
    }

   

    public String getCodi() {
        return codi;
    }

    public void setCodi(String codi) {
        this.codi = codi;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public Date getData() {
        return data;
    }

    public void setData(Date data) {
        this.data = data;
    }

    public int getHores() {
        return hores;
    }

    public void setHores(int hores) {
        this.hores = hores;
    }

    public Set<Profesor> getProfesores() {
        return profesores;
    }

    public void setProfesores(Set<Profesor> profesores) {
        this.profesores = profesores;
    }

    public Aula getAula() {
        return aula;
    }

    public void setAula(Aula aula) {
        this.aula = aula;
    }
    
    
}
